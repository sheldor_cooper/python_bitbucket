The source code contained in the 'src' src folder are a consolidation of many of the tree fucntions.

'PY_BinaryTree_0.py':
This file contains a purely, what I call, a student version of the tree implementtation. It means that
the code is structured more in a c-programming fashion. The node of the tree is considered as a class
(something that would be a structure(struct) in C). All the other functionality is present in the form
of global functions, just like in C.

Moreover, the code highlights the syntactic variations of the language that can be seen while we code
the same algorithms. For example, the two 'inorder traversal' functions:
	IO_list_1()  and
	IO_list_2()
are performing the same task of storing the inorder traversal in a vector/array/list, but the way they
are implemented are a syntactic variation highlighting the different ways that the code can be written
in the language.

All the functions of the form 'x_fast()', for example - IO_fast(), PO_fast()...etc - are not actually
faster, they are just the written taking the advantage of the condensation charateristic of Python just
to show another variation in the way Language can be written.