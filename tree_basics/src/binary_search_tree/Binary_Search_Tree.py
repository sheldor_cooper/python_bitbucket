# The left subtree will contain all the elements that are smaller than the current node's data in some sense.
# The right subtree will contain all the elements that are larger than the current node's data in some sense or are equal to current node's data.

class BST_Node(object):
	def __init__(self, data=None):
		self.data  = data
		self.left  = None
		self.right = None

	def getData(self):
		if self is None:
			return 'NILL'
		else:
			return self.data
	def setData(self, data):
		self.data = data

	def getLeft(self):
		return self.left
	def setLeft(self, node):
		self.left = node

	def getRight(self):
		return self.right
	def setRight(self, node):
		self.right = node

# A class for the Binary Search Tree
class BST(object):
	def __init__(self):
		self.root = None

	def getRoot(self):
		return self.root
	
	# Not sure, if required or not.
	def setRoot(self, node):
		self.root = node

	# Fill in Level Order fashion
	def FLO(self, curr_node, arr, arr_size, i):
		if i<arr_size:
			curr_node = BST_Node(arr[i])
			if self.root is None:
				self.setRoot(curr_node)
			curr_node.setLeft(self.FLO(curr_node.getLeft(), arr, arr_size, 2*i+1))
			curr_node.setRight(self.FLO(curr_node.getRight(), arr, arr_size, 2*i+2))
		return curr_node

	def search_rec(self, curr_node, key):
		if curr_node is None or curr_node.getData() == key:
			return curr_node

		elif key < curr_node.getData():
			return self.search_rec(curr_node.getLeft(), key)

		else:
			return self.search_rec(curr_node.getRight(), key)

	# Modify the search function
	def insert_rec(self, curr_node, data):
		if curr_node is None:
			curr_node = BST_Node(data)
			if self.root is None:
				self.setRoot(curr_node)
		else:
			if data < curr_node.getData():
				if curr_node.getLeft() is None:
					curr_node.setLeft(BST_Node(data))
				else:
					self.insert_rec(curr_node.getLeft(), data)
			
			else: # data >= curr_node.getData()
				if curr_node.getRight() is None:
					curr_node.setRight(BST_Node(data))
				else:
					self.insert_rec(curr_node.getRight(), data)

	def IO_print(self, curr_node):
		if curr_node:
			self.IO_print(curr_node.getLeft())
			print(curr_node.getData(), end=' ')
			self.IO_print(curr_node.getRight())


def main():
	arr = [8, 10, 3, 14, 6, 7, 1, 4, 13]
	tree2 = BST()
	tree2.FLO(tree2.getRoot(), arr, len(arr), 0)
	tree1 = BST()
	for a in arr:
		tree1.insert_rec(tree1.getRoot(), a)
	tree2.IO_print(tree2.getRoot())

	print(tree1.search_rec(tree1.getRoot(), 5).getData())

if __name__ == '__main__':
	main()