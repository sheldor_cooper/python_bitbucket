# Node/Tree is present as a class with a Constructor to create a new node/Tree

# all other functionality is present as non-member functions
# Just like C
# Highly  Student level Code.







# tree class
class binary_tree(object):
	def __init__(self,data):
		self.data=data
		self.l=None
		self.r=None

# Create the tree from an existing array
# FIll the tree in BFS fashion (Level ORder)
# FIll the tree in one go if you have a list of data
def FLO(curr_tree, arr, arr_size, i):
	if i<arr_size:
		curr_tree=binary_tree(arr[i])
		curr_tree.l = FLO(curr_tree.l, arr, arr_size, 2*i+1)
		curr_tree.r = FLO(curr_tree.r, arr, arr_size, 2*i+2)
	return curr_tree

def IO_print(curr_tree):
	if curr_tree:
		IO_print(curr_tree.l)
		print(curr_tree.data, end=' ')
		IO_print(curr_tree.r)

# Stores the inorder Traversal in a list
#[https://stackoverflow.com/questions/41622174/inorder-binary-tree-traversal-using-python?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa]
def IO_list_1(curr_tree):
	res=[]
	if curr_tree:
		res+=IO_list_1(curr_tree.l)
		res+=[curr_tree.data]
		res+=IO_list_1(curr_tree.r)
	return res

# Stores the inorder Traversal in a list: Code Variant
def IO_list_2(curr_tree,vec):
	if curr_tree:
		IO_list_2(curr_tree.l,vec)
		vec+=[curr_tree.data]
		IO_list_2(curr_tree.r,vec)

# The Commented part shows the code that saves the InOrder as a List if all three of those lines are uncommented.
def IO_itr(curr_tree):
	if curr_tree:
		t,stk,finished=curr_tree,[],False
		#l=[] #optional (if you want to store the result in a list)
		while not finished:
			if t is not None:
				stk.append(t)
				t = t.l
			else:
				if len(stk)!=0:
					t = stk.pop()
					print(t.data,end=' ')
					#l.append(t.data) # if you want to store the result in a list
					t = t.r
				else:
					finished=True
		#return l

# A condensed form of code
def IO_fast(curr_tree):
	return (IO_fast(curr_tree.l) + [curr_tree.data] + IO_fast(curr_tree.r) if curr_tree else [])

# Mirror Inorder: R-V-L
def mirror_IO_print(curr_tree):
	if curr_tree:
		mirror_IO_print(curr_tree.r)
		print(curr_tree.data, end=' ')
		mirror_IO_print(curr_tree.l)

def mirror_IO_list_1(curr_tree):
	res=[]
	if curr_tree:
		res+=mirror_IO_list_1(curr_tree.r)
		res+=[curr_tree.data]
		res+=mirror_IO_list_1(curr_tree.l)
	return res

# passing the vector that stores the traversal as a parameter.
def mirror_IO_list_2(curr_tree, vec):
	if curr_tree:
		mirror_IO_list_2(curr_tree.r,vec)
		vec+=[curr_tree.data]
		mirror_IO_list_2(curr_tree.l,vec)

# The Commented part shows the code that saves the InOrder as a List if all three of those lines are uncommented.
def mirror_IO_itr(curr_tree):
	if curr_tree:
		t,stk,finished=curr_tree,[],False
		#l=[]
		while not finished:
			if t is not None:
				stk.append(t)
				t=t.r
			else:
				if len(stk)!=0:
					t=stk.pop()
					print(t.data,end=' ')
					#l.append(t.data)
					t=t.l
				else: finished=True
		#return l

def mirror_IO_fast(curr_tree):
	return (mirror_IO_fast(curr_tree.r) + [curr_tree.data] + mirror_IO_fast(curr_tree.l)) if curr_tree else []



# PreOrder All 4 forms
def PO_print(curr_tree):
	if curr_tree:
		print(curr_tree.data, end=' ')
		PO_print(curr_tree.l)
		PO_print(curr_tree.r)

def PO_list_1(curr_tree):
	res=[]
	if curr_tree:
		res+=[curr_tree.data]
		res+=PO_list_1(curr_tree.l)
		res+=PO_list_1(curr_tree.r)
	return res

def PO_list_2(curr_tree,vec):
	if curr_tree:
		vec+=[curr_tree.data]
		PO_list_2(curr_tree.l, vec)
		PO_list_2(curr_tree.r, vec)

def PO_itr(curr_tree):
	if curr_tree:
		t,stk,finished = curr_tree,[],False
		#l=[]
		while not finished:
			if t is not None:
				print(t.data,end=' ')
				#l.append(t.data)
				stk.append(t)
				t=t.l
			else:
				if len(stk)!=0:
					t = stk.pop()
					t = t.r
				else: finished=True
		#return l

def PO_fast(curr_tree):
	return ([curr_tree.data] + PO_fast(curr_tree.l) + PO_fast(curr_tree.r) if curr_tree else [])

def mirror_PO_print(curr_tree):
	if curr_tree:
		print(curr_tree.data, end=' ')
		mirror_PO_print(curr_tree.r)
		mirror_PO_print(curr_tree.l)

def mirror_PO_list_1(curr_tree):
	res=[]
	if curr_tree:
		res+=[curr_tree.data]
		res+=mirror_PO_list_1(curr_tree.r)
		res+=mirror_PO_list_1(curr_tree.l)
	return res

def mirror_PO_list_2(curr_tree,vec):
	if curr_tree:
		vec+=[curr_tree.data]
		mirror_PO_list_2(curr_tree.r,vec)
		mirror_PO_list_2(curr_tree.l,vec)

def mirror_PO_itr(curr_tree):
	if curr_tree:
		t,stk,finished=curr_tree,[],False
		#l=[]
		while not finished:
			if t is not None:
				print(t.data,end=' ')
				#l.append(t.data)
				stk.append(t)
				t=t.r
			else:
				if len(stk)!=0:
					t=stk.pop()
					t=t.l
				else:
					finished = True
		#return l

def mirror_PO_fast(curr_tree):
	return ([curr_tree.data] + mirror_PO_fast(curr_tree.r) + mirror_PO_fast(curr_tree.l)) if curr_tree else []




# PostOrder All 4 Forms
def PTO_print(curr_tree):
	if curr_tree:
		PTO_print(curr_tree.l)
		PTO_print(curr_tree.r)
		print(curr_tree.data, end=' ')

def PTO_list_1(curr_tree):
	res=[]
	if curr_tree:
		res+=PTO_list_1(curr_tree.l)
		res+=PTO_list_1(curr_tree.r)
		res+=[curr_tree.data]
	return res

def PTO_list_2(curr_tree,vec):
	if curr_tree:
		PTO_list_2(curr_tree.l, vec)
		PTO_list_2(curr_tree.r, vec)
		vec+=[curr_tree.data]

'''
Preorder: 1 2 4 5 3 6 7

Mirror-Preorder: 1 3 7 6 2 5 4

Reverse of Mirror-Preorder = POSTORDER = 4 5 2 6 7 3 1


'''
# PTO = reversed(mirror_PO)
def PTO_itr(curr_tree):
	if curr_tree:
		t,stk,finished=curr_tree,[],False
		l=[]
		while not finished:
			if t is not None:
				l.append(t.data)
				stk.append(t)
				t=t.r
			else:
				if len(stk)!=0:
					t = stk.pop()
					t=t.l
				else: finished=True
		return l[::-1]
	#return mirror_PO_fast(curr_tree)[::-1]

def PTO_fast(curr_tree):
	return (PTO_fast(curr_tree.l) + PTO_fast(curr_tree.r) + [curr_tree.data] if curr_tree else [])

def mirror_PTO_print(curr_tree):
	if curr_tree:
		mirror_PTO_print(curr_tree.r)
		mirror_PTO_print(curr_tree.l)
		print(curr_tree.data,end=' ')

def mirror_PTO_list_1(curr_tree):
	res=[]
	if curr_tree:
		res+=mirror_PTO_list_1(curr_tree.r)
		res+=mirror_PTO_list_1(curr_tree.l)
		res.append(curr_tree.data)
	return res

def mirror_PTO_list_2(curr_tree,vec):
	if curr_tree:
		mirror_PTO_list_2(curr_tree.r,vec)
		mirror_PTO_list_2(curr_tree.l,vec)
		vec+=[curr_tree.data]


# Height: Use PostOrder
def height_1(curr_tree):
	if curr_tree is None: return 0
	else:
		left_height = height_1(curr_tree.l)
		right_height= height_1(curr_tree.r)
		return max(left_height, right_height)+1

# no need to write the else
def height_2(curr_tree):
	if curr_tree is None: return 0
	left_height = height_2(curr_tree.l)
	right_height= height_2(curr_tree.r)
	return max(left_height, right_height)+1

# swapping the if and else conditions
def height_3(curr_tree):
	if curr_tree:
		left_height = height_3(curr_tree.l)
		right_height= height_3(curr_tree.r)
		return max(left_height, right_height)+1
	else: return 0

# again no need to write the else
def height_4(curr_tree):
	if curr_tree:
		left_height = height_4(curr_tree.l)
		right_height= height_4(curr_tree.r)
		return max(left_height, right_height)+1
	return 0

def height_fast(curr_tree):
	return 1+max(height_fast(curr_tree.l), height_fast(curr_tree.r)) if curr_tree else 0

# Print the current Level
# Use Preorder Traversal, Limit the print by the level number
def print_current_level(curr_tree, level):
	if curr_tree is None: return
	elif level==1: print(curr_tree.data,end=' ')
	elif level>1:
		print_current_level(curr_tree.l,level-1)
		print_current_level(curr_tree.r,level-1)


# BFS Method One:
def BFS(curr_tree):
	[print_current_level(curr_tree,i) for i in range(1,height_fast(curr_tree)+1)]

#BFS Method 2:
# Use a Queue, Interative
def BFS_itr(curr_tree):
	if curr_tree:
		q=[]
		q.append(curr_tree)
		while len(q)!=0: #while the queue is not empty
			print(q[0].data,end=' ')
			if q[0].l is not None: q.append(q[0].l)
			if q[0].r is not None: q.append(q[0].r)
			del(q[0])


# Printing the current Level iteratively: Modify the iterative BFS Traversal
def print_current_level_itr(curr_tree,level):
	if curr_tree is None: q.append(('None',level))
	else: #BFS
		q=[]
		q.append((curr_tree, level)) # Push the node and its level into the queue as a tuple.

		# Start counting in the reverse order (5,4,3,...)
		while q[0][1]!=0: # while you have not crossed in the target level (target level => Count=1)
			
			# if you are at the target level.
			if q[0][1]==1:
				# if the node is none, print(none), else print the data in the node.
				if q[0][0]=='None': print('None')
				else: print(q[0][0].data,end=' ')
			
			l=q[0][1]-1 #counting the reverse order
			
			# BFS Standard Procedure: Append whatever is on the left of the current node to the queue.
			if q[0][0].l is None: q.append(('None',l))
			else: q.append((q[0][0].l,l))
			
			# BFS Standard Procedure: Append whatever is on the left of the current node to the queue.
			if q[0][0].r is None: q.append(('None',l))
			else: q.append((q[0][0].r,l))

			# BFS standard: Pop the queue from the front.
			del q[0]

def BFS_search_itr(curr_tree, key):
	"""
		Returns the pointer that has the key if found,
		else returns False
	"""
	if curr_tree is None: return False
	else:
		q=[]
		q.append(curr_tree)
		while len(q)!=0:
			# If cu
			if q[0].data == key:
				return q[0]

			if q[0].l is not None:
				q.append(q[0].l)

			if q[0].r is not None:
				q.append(q[0].r)

			del q[0]
		return False

# interative BFS is used.
def ILO_itr(curr_tree, data):
	if curr_tree is None: curr_tree=binary_tree(data)
	else:
		q=[]
		q.append(curr_tree)
		while len(q)!=0:
			if q[0].l is not None and q[0].r is not None:
				q.append(q[0].l)
				q.append(q[0].r)
				del q[0]
			elif q[0].l is not None and q[0].r is None:
				q[0].r = binary_tree(data)
				break
			else:
				q[0].l=binary_tree(data)
				break


# Get all the paths
# Pre-Order is to be slightly modified, Since you want to visit every node and record it.
# refer to the PO_print() funstion above.
def print_all_paths(curr_tree, path):
	if curr_tree:
		path.append(curr_tree.data)
		if curr_tree.l is None and curr_tree.r is None: # check if the current node is the leaf node.
			print(path)
		print_all_paths(curr_tree.l,path[:])
		print_all_paths(curr_tree.r,path[:])


def main():
	arr=[1,2,3,4,5,6,7,8,9]
	root = None
	root = FLO(root,arr,len(arr),0)

	print('\n\n--- INORDERs---')
	print("IO_print = ", end=''); IO_print(root); print()
	print('IO_list_1 =', IO_list_1(root))
	vec=[]; IO_list_2(root, vec); print('IO_list_2 =', vec)
	print('IO_fast =', IO_fast(root))
	print('IO_itr = ',end=''); IO_itr(root)
	print('\nmirror_IO_print = ', end=''); mirror_IO_print(root); print()
	print('mirror_IO_list_1 =', mirror_IO_list_1(root))
	vec=[]; mirror_IO_list_2(root,vec); print('mirror_IO_list_2 =', vec)
	print('mirror_IO_itr = ', end=''); mirror_IO_itr(root); print()
	print('mirror_IO_fast =', mirror_IO_fast(root))

	print('\n\n--- PREORDERs---')
	print('PO_print = ', end=''); PO_print(root); print()
	print('PO_list_1 =', PO_list_1(root))
	vec=[]; PO_list_2(root, vec); print('PO_list_2 =', vec)
	print('PO_fast =', PO_fast(root))
	print('PO_itr = ', end=''); PO_itr(root)
	print('\nmirror_PO_print = ', end=''); mirror_PO_print(root); print()
	print('mirror_PO_list_1 =', mirror_PO_list_1(root))
	vec=[]; mirror_PO_list_2(root,vec); print('mirror_PO_list_2 =', vec)
	print('mirror_PO_itr = ', end=''); mirror_PO_itr(root); print()
	print('mirror_PO_fast =', mirror_PO_fast(root))

	print('\n\n--- POSTORDERs---')
	print('PTO_print = ', end=''); PTO_print(root); print()
	print('PTO_list_1 =', PTO_list_1(root))
	vec=[]; PTO_list_2(root, vec); print('PTO_list_2 =', vec)
	print('PTO_itr =', PTO_itr(root))
	print('PTO_fast =', PTO_fast(root))
	print('mirror_PTO_print = ',end=''); mirror_PTO_print(root); print()
	print('mirror_PTO_list_1 =', mirror_PTO_list_1(root))
	vec=[]; mirror_PTO_list_2(root,vec); print('mirror_PTO_list_2 =',vec)

	print('\n\n--- HEIGHT ----')
	print(height_1(root))
	print(height_2(root))
	print(height_3(root))
	print(height_4(root))
	print(height_fast(root))

	print('\n\n--- Printing a particular Level ---')
	print('Level-3: ',end=''); print_current_level(root,3); print()
	print('Level-4: ',end=''); print_current_level(root,4); print()
	print('Level-3 itr: ',end=''); print_current_level_itr(root,3)

	print('\nBFS: Method-1: Recursive Print Current Level: ',end=''); BFS(root)
	print('\nBFS_itr: ',end=''); BFS_itr(root); print('\n')

	print(BFS_search_itr(root,1).data)
	print(BFS_search_itr(root,4).data)
	print(BFS_search_itr(root,10))
	print('\n\n')

	'''
                1
            /       \
           2         3
        /     \    /   \
       4       5  N     7
      / \     / \      / \
     6   N   N   N    8   N

		So, the first insert should take place at left of '3', and
		the second insert should take place at the right of '4'
	'''

	root1 = binary_tree(1)
	root1.l = binary_tree(2)
	root1.r = binary_tree(3)
	root1.l.l = binary_tree(4)
	root1.l.l.l = binary_tree(6)
	root1.l.r = binary_tree(5)
	root1.r.r = binary_tree(7)
	root1.r.r.l = binary_tree(8)

	print('Initially, Tree =', IO_fast(root1))
	ILO_itr(root1,10)
	print('After inserting 10, Tree =', IO_fast(root1))
	ILO_itr(root1,20)
	print('After inserting 20, Tree =', IO_fast(root1))


	print('All the root to leaf paths are: ')
	print_all_paths(root, [])

if __name__ == '__main__':
	main()