# quat_node = Node for the Quaternary tree
class quat_node(object):
	def __init__(self, data=None):
		self.d  = data
		self.l  = None
		self.m1 = None
		self.m2 = None
		self.r  = None

def FLO(curr_node, arr, i, arr_size):
	if i<arr_size:
		curr_node  = quat_node(arr[i])
		curr_node.l  = FLO(curr_node.l,  arr, 4*i+1, arr_size)
		curr_node.m1 = FLO(curr_node.m1, arr, 4*i+2, arr_size)
		curr_node.m2 = FLO(curr_node.m2, arr, 4*i+3, arr_size)
		curr_node.r  = FLO(curr_node.r,  arr, 4*i+4, arr_size)
	return curr_node

def IO_print(curr_node):
	if curr_node:
		IO_print(curr_node.l)
		print(curr_node.d, end=' ')
		IO_print(curr_node.m1)
		IO_print(curr_node.m2)
		IO_print(curr_node.r)

def IO_itr(curr_node):
	if curr_node:
		stk, finished, mv = [], False, False
		while not finished:
			if curr_node is not None:
				stk.append(curr_node)
				curr_node = curr_node.l
			else:
				if len(stk)!=0:
					curr_node = stk.pop()
					print(curr_node.d, end=' ')
					mv = False
					if curr_node.r is not None:
						stk+=[curr_node.r]
					if curr_node.m2 is not None:
						stk+=[curr_node.m2]
					curr_node = curr_node.m1
					mv = True
				else:
					finished = True

def main():
	arr = [1,2,3,4,5,6,7,8]

	root = None
	root = FLO(root, arr, 0, len(arr))

	IO_print(root); print()
	IO_itr(root)

if __name__=='__main__':
	main()