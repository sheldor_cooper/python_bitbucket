# ter_node: node of a ternary tree
class ter_node(object):
	def __init__(self, data=None):
		self.d = data
		self.l = None
		self.m = None
		self.r = None

# Fill the ternary tree in level order
def FLO(curr_node, arr, i, arr_size):
	if i<arr_size:
		curr_node = ter_node(arr[i])
		curr_node.l = FLO(curr_node.l, arr, 3*i+1, arr_size)
		curr_node.m = FLO(curr_node.m, arr, 3*i+2, arr_size)
		curr_node.r = FLO(curr_node.r, arr, 3*i+3, arr_size)
	return curr_node

def IO_print(curr_node):
	if curr_node:
		IO_print(curr_node.l)
		print(curr_node.d, end=' ')
		IO_print(curr_node.m)
		IO_print(curr_node.r)

def IO_list_1(curr_node):
	res=[]
	if curr_node:
		res += IO_list_1(curr_node.l)
		res.append(curr_node.d)
		res += IO_list_1(curr_node.m)
		res += IO_list_1(curr_node.r)
	return res

def IO_list_2(curr_node, vec):
	if curr_node:
		IO_list_2(curr_node.l, vec)
		vec += [curr_node.d]
		IO_list_2(curr_node.m, vec)
		IO_list_2(curr_node.r, vec)

def IO_itr(curr_node):
	if curr_node:
		stk, finished, mv = [], False, False
		while not finished:
			if curr_node is not None:
				stk+=[curr_node]
				curr_node = curr_node.l
			else:
				if len(stk)!=0:
					curr_node = stk.pop()
					print(curr_node.d, end=' ')
					mv=False
					if mv == False:
						if curr_node.r is not None:
							stk+=[curr_node.r]
						curr_node = curr_node.m
						mv = True
				else:
					finished =True

def main():
	arr = [1,2,3,4,5,6,7,8,9,10,11,12,13]
	#arr=[1,2,3,4]

	root = None
	root = FLO(root, arr, 0, len(arr))

	IO_print(root); print()
	print(IO_list_1(root))
	vec = []; IO_list_2(root, vec); print(vec)
	IO_itr(root)

if __name__ == '__main__':
	main()